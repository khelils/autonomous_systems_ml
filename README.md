# AI for Autonomous Systems (ASI 3A and M2 MARS program): Machine Learning course
## `News`

- First course session will take place Monday, September 26 (G-2B010, 8:30)

- **All lab works must be prepared in advance**. Preparation and assigned homework will be specified a few days before each lab session.


- **Due to a lack of computer rooms, lab work will sometimes take place in rooms that are not equipped. Students are invited to bring their own laptops to lab work sessions. You will find below all necessary instructions to run the lab work codes on your own computer.**

`Lab timetables, 2022-2023`

|Group | Supervisor | Members | Lab1 | Lab2 | Lab3| Lab4| Lab5 | Lab6 | Lab7 | Lab8 | Lab9 |
|------|:-----------|---------|------|------|-----|-----|------|------|------|------|-------|
|**G1** |  F.Chatelain |**ASI** students: from AARAIB to LERAY, french speaking  | 3 oct | 17 oct | 7 nov | 21 nov| 5 dec | 19 dec | 9 jan |  16 jan | 23 jan|
|**G2** | H. Lebeau |**ASI** students: from LIN to YANG+JEAGLY, french speaking  | 3 oct | 17 oct | 7 nov | 21 nov| 5 dec | 19 dec | 9 jan |  16 jan | 23 jan|
|**G3** | O. Michel |**MARS**+foreign students - english speaking                | 3 oct | 17 oct | 7 nov | 21 nov| 5 dec | 19 dec | 9 jan |  16 jan | 23 jan|



<!--
## `News`

- Because of the containment, there is a zoom link (see the [chamilo page](https://chamilo.grenoble-inp.fr/courses/ENSE3WEUMAIA0/index.php?) of the course) to participate in videoconference to the class every monday morning from 8:30 to 12:45. ___WARNING___ : Pr. CHATELAIN and MICHEL use different zoom link. Please refer to [Chamilo page](https://chamilo.grenoble-inp.fr/courses/ENSE3WEUMAIA0/index.php?).

##### Monday, January 11th 2021
 This last lectures will be focused on CNN and AE principles. The correspoding slides are to be found at the end of the lectures on MLPC_NN used last week.  In a second part, a brief overview on Recurent networks will be given.
 The lectre will befin at 10. Note that corresponding notebooks are given for illustration. No report are required for this session.


##### Lab6 (Monday, January 4th, 2021) instructions

- Lab6 Notebooks on Perceptrons are[here]((https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks/10_NN_MLPC).
- Upload at the end of the session your lab 6 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Homework for Monday, January 4th, 2021
- read the lesson on Perceptron, MultiLayer perceptron and Neural Networks (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/10_MLPC_NN_2020.pdf)). Slides 28 to 37 may be skipped infirst reading.
- Prepare your questions for the course and lab sessions.

##### Lab5 (Monday, December 14th) instructions

- Lab5 Notebooks on Trees and Boosting are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks/9_Trees_Boosting).
- Upload at the end of the session your lab 5 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Homework for Monday, December 14th

- read the lesson on trees and Bossting (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/Trees_RandomForest_Boosting.pdf)). Slides 28 to 37 may be skipped infirst reading.
- Prepare your questions for the course and lab sessions.

##### Lab4 (Monday, December 7) instructions

- Lab4 statement on SVM and clustering is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab4_statement.md)
- Upload at the end of the session your lab 4 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=143765&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### ~~Homework for Monday, December 7~~

- ~~read the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/7_support_vector_machines.pdf)) on support vector machines:~~
  - ~~you can *skip the slides 10 to 18* (on constrained convex optimization) in first reading,~~
  - ~~*read up to slide 42* (the introduction to random forest in appendix is optional)~~
- ~~read the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf)) on clustering: *read up to model selection slide 30*~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab3 (Monday, November 30) instructions~~

- ~~Lab3 statement on generalized linear models, regularization and variable selection is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab3_statement.md)~~
- ~~Upload at the end of the session your lab 3 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=139477&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Homework for Monday, November 30~~

- ~~read the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on linear models and regularization.~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab2 (Monday, November 23) instructions~~

- ~~Lab2 statement on Dicriminant Analysis (LDA/QDA), Naive Bayes and Principal Component Analysis is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab2_statement.md)~~
- ~~Upload at the end of the session your lab 2 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=138838&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Homework for Monday, November 23~~

- ~~read the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/4_discriminant_analysis.pdf)) on generative models: discriminant analysis + naïve Bayes~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab1 (Monday, November 16) instructions~~

- ~~Lab1 statement on ML basics, k-NN and model assesment is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab1_statement.md)~~
- ~~Upload at the end of the session your lab 1 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=134377&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

-->


<!--

##### Lab9

 The last lab session  will be devoted to revisions. You will be able to work on the notebooks that you did not have time to study in the previous sessions, and tackle the questions of your choice.


##### Lab8

- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/10_MLPC_NN_2020.pdf)) on MultiLayer Perceptrons and Neural Networks.
- Lab8 statement on neural nets and deep convolutional networks is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab8_statement.md)
- **Except if your supervisor give you another instruction** upload at the end of the session your lab 8 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=204165) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lab7

- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/9_Trees_RandomForest_Boosting.pdf)) on Classification and Regression Trees, tree pruning, Random Forests.
- Lab7 statement on Tree based method for unsupervised classification and regression problems is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab7_statement.md)
- **Except if your supervisor give you another instruction** upload at the end of the session your lab 7 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=203024&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lab6

- ~~**HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf)) on clustering methods, with focus on K-Means and EM algorithm. Read slides 1 to 37.~~
- ~~Lab6 statement on clustering methods is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab6_statement.md)~~
- ~~**Except if your supervisor give you another instruction** upload at the end of the session your lab 6 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=153447&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Lab5~~

- ~~**HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on the lasso regularization and logistic regression parts.~~
- ~~Lab5 statement on generalized linear models, lasso regulariation and variable selection is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab5_statement.md)~~
- ~~**Except if your supervisor give you another instruction** upload at the end of the session your lab 5 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Lab4~~

- ~~**HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on linear models until ridge regularization.~~
- ~~Lab4 statement on linear models and ridge regression is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab4_statement.md).~~
- ~~**Except if your supervisor give you another instruction**  upload at the end of the session your lab 4 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=143765&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~


##### ~~Lab3~~

- ~~Lab3 statement on Principal Component Analysis (PCA) is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab3_statement.md)~~
- ~~**Except if your supervisor give you another instruction** upload at the end of the session your lab 3 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=139477&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~


##### ~~Lab2 (Monday, October 18 for the groups of students supervised by Khaoula Tidriri and Florent Chatelain)~~

- ~~Lab2 statement on Discriminant Analysis (LDA/QDA) and Naive Bayes is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab2_statement.md)~~
- ~~Upload at the end of the session your lab 2 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=138838&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Lecture (Monday, October 11) on linear models and regularization~~

- ~~Slides on linear models and regularization are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf).~~

##### ~~Lab1 (Monday, October 4) instructions~~

- ~~Lab1 statement on ML basics, k-NN and model assesment is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab1_statement.md)~~
- ~~Upload at the end of the session your lab 1 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=134377&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~
-->


##### Homework for Monday, October 3rd

- read/run the [introductory notebooks to python](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks%2F0_python_in_a_nutshell)  (especially for those who are not comfortable with python)
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/3_model_assesment.pdf)) on model assessment and validation
- **finish reading the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/2_illustrations_knn.pdf)) on the k-NN example from slide 7 to 12
- prepare your questions for the course/lab session!



## Welcome to the Machine Learning course!

You will find in this gitlab repository the necessary material for the teaching of _Machine Learning_:

- course materials for the lessons ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/slides))
- examples and exercises for the labs in the form of [Jupyter python notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks) (`.ipynb` files) and/or via online applications,
- quiz: [Socrative](https://b.socrative.com/login/student/) online tool, room *MLASI*

These resources will be updated as the sessions progress.

### How to use the notebooks?

The examples and exercises will be done under python 3.x through [scikit-learn](https://scikit-learn.org/), and also [tensorflow](https://www.tensorflow.org/). These are two of the most widely used machine learning packages.

The _Jupyter Notebooks_ (`.ipynb` files) are programs containing both cells of code (for us Python) and cells of markdown text for the narrative side. These notebooks are often used to explore and analyze data. Their processing is done with a `jupyter-notebook`, or `juypyter-lab` application, which is accessed through a web browser.

In order to run them you have several possibilities:

1. Download the notebooks to run them on your machine. This requires a Python environment (> 3.3), and the Jupyter notebook and scikit-learn packages. It is recommended to install them via the [anaconda](https://www.anaconda.com/downloads) distribution which will directly install all the necessary dependencies.

**Or**

2. Use a `jupyterhub` online service:

  - we recommend the UGA's service, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr), so that you can run your notebooks on the UGA's computation server while saving your modifications and results. Also useful to launch a background computation (connection with your Agalan account; requires uploading your notebooks+data to the server).
  - alternatively you can use an equivalent `jupyterhub` service. For example the one from google, namely [google-colab](https://colab.research.google.com/), which allows you to run/save your notebooks and also to _share the edition to several collaborators_ (requires a google account and upload your notebooks+data in your Drive)

**Or**

3. Use the _mybinder_ service ans links to run them interactively and remotely (online): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fai-courses%2Fautonomous_systems_ml/master?urlpath=lab/tree/notebooks) (open the link and wait a few seconds for the environment to load).<br>
  **Warning:** Binder is meant for _ephemeral_ interactive coding, meaning that your own modifications/codes/results will be lost when your user session will automatically shut down (basically after 10 minutes of inactivity)



**Note :** You will also find among the notebooks an introduction to Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks%2F0_python_in_a_nutshell)

### Miscellaneous remarks on the materials

- The slides are designed to be self-sufficient (even if the narrative side is often limited by the format).
- In addition to the slides and bibliographical/web references, we generally propose links or videos (at the beginning or end of the slides) specific to the concepts presented. These lists are of course not exhaustive, and you will find throughout the web many resources, often pedagogical. Feel free to do your own research. <!-- and share it on the [Riot room](https://riot.ensimag.fr/#/room/#sicom-ml:ensimag.fr) if you find it useful. -->

<!-- - [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/54301940e4486a8ece22a910c3efa1b2734ed82d?filepath=notebooks) link to run the examples, *except Deep learning ones* too computationally demanding for the JupyterHub server (use the first solution to run these notebooks with your own ressources...) -->
